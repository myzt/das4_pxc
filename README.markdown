# DAS4 PXC 

Command line tool to control the [DAS4] [PXC].

# Dependencies

- python 2.6 or 2.7
- pySerial
- argparse (for python 2.6 it's included in 2.7)

Both are can be installed using easy_install or pip.

# Installation

Just clone and cross fingers.

# Usage
    usage: das4pxc [-h] [-n] [-t TOPO] [-s Source] [-d Destination] [-a {1,2,3,4}]
                   [-c [{19,20,21,22} [{19,20,21,22} ...]]] [-v]

    Source Destination to Topology

    optional arguments:
      -h, --help            show this help message and exit
      -n, --nocheck         don't show current topology (useful when switch is in
                            an unknown state)
      -t TOPO, --topo TOPO  sets PXC topology
      -s src, --source src
      -d dst, --destination dst
      -a {1,2,3,4}, --amount {1,2,3,4}
                            max_links=4
      -c [{19,20,21,22} [{19,20,21,22} ...]], --channels [{19,20,21,22} [{19,20,21,22} ...]]
                            Wavelengths: 19, 20, 21, 22
      -v, --verbose

# Examples

    $ ./das4pxc --topo 3 -v
    Current state: topology 3
    Setting new topology to 3
    Topology change took: 0.531029939651 seconds
    New state: topology 3
    
    {'SN039874': [['19', '2', '0.0'], ['21', '3', '0.0'], ['22', '2', '0.0']],
     'SN042680': [['19', '1', '0.0'], ['21', '4', '0.0'], ['22', '1', '0.0']],
     'SN043946': [['21', '1', '0.0'], ['22', '4', '0.0']],
     'SN044017': [['21', '2', '0.0'], ['22', '3', '0.0']]}
    
    $

    $ ./das4pxc -s UVA -d ASTRON -c 22
    Current state, non-default topology:
    {'SN039874': [],
     'SN042680': [['22', '4', '0.0']],
     'SN043946': [],
     'SN044017': [['22', '2', '0.0']]}
    Setting new topology: UVA to ASTRON
    Topology change took: 0.00384783744812 seconds
    New state,  non-default topology:
    {'SN039874': [],
     'SN042680': [['22', '4', '0.0']],
     'SN043946': [],
     'SN044017': [['22', '2', '0.0']]}
    $

[DAS4]: http://www.cs.vu.nl/das4/
[PXC]: http://www.surfnet.nl/nl/Innovatieprogramma%27s/gigaport3/Documents/GP3-2010-PHO-8R-Gamage-GovBrd.pdf 
