import pprint, inspect, datetime, math, re
from timeit import Timer
from random import choice

from twisted.python import log
from twisted.internet import defer

from opennsa import error, state # interface
from opennsa.backends.common import scheduler #, simplebackend

from das4_pxc import make_topo as makeTopology
from das4_pxc.pxc import pxc


# connecties tussen stps
connecties = []
# lijst van golflengtes in gebruik
w_in_use = []
w_not_used = [19, 20, 21, 22]

conn = ""

nr_used_links = 0
source_name = "" 



# between 2 stps within 1 network
class DASConnection:
    
    # number of links?    
    
    def __init__(self, pxc,source_port, dest_port, service_parameters, network_name, cal, log_system, connection_manager):
        self.source_port = source_port              # "VU_A1" (NRM)
        self.dest_port = dest_port
        self.service_parameters = service_parameters # stp, time, bandw
        self.network_name = network_name
        self.calendar = cal

        self.state = state.ConnectionState()
        self.scheduler = scheduler.TransitionScheduler()
        
        self.log_system = log_system
        self.connection_manager = connection_manager
        
        self.pxc = pxc
        connecties.append(self)
        self.wavelengths = []
        
    def curator(self):
        return 'DAS NRM'


    def stps(self):
        return self.service_parameters.source_stp, self.service_parameters.dest_stp

    def logStateUpdate(self, state_msg):
        log.msg('Link: %s, %s -> %s : %s.' % (id(self), self.source_port, self.dest_port, state_msg), system=self.log_system)

    def reserve(self):
        global w_not_used
        global w_in_use
        global nr_used_links
        flag = False
        def scheduled(st):
            self.state.switchState(state.SCHEDULED)
            self.scheduler.scheduleTransition(self.service_parameters.end_time, lambda _ : self.terminate(), state.TERMINATING)
            self.logStateUpdate('SCHEDULED')

        try:
            self.state.switchState(state.RESERVING)
            self.logStateUpdate('RESERVING')
            self.state.switchState(state.RESERVED)
        except error.StateTransitionError:
            return defer.fail(error.ReserveError('Cannot reserve connection in state %s' % self.state()))

# STPS <STP DAS4:UVA20> T <STP DAS4:Astron20>
        src,dest = self.stps()
        src = str(src)
        dest = str(dest)

        sr = re.findall("\d+", src)
        ds = re.findall("\d+", dest)

        try:
            if int(ds[-1]) in w_in_use:
                for w in self.wavelengths:
                    if w in w_in_use:
                        w_in_use.remove(w)
                    if w not in w_not_used:
                        w_not_used.append(w)
                self.calendar.removeConnection(self.source_port, self.service_parameters.start_time, self.service_parameters.end_time)
                self.calendar.removeConnection(self.dest_port  , self.service_parameters.start_time, self.service_parameters.end_time)
                raise error.ReserveError()
        except error.ReserveError:
            return defer.fail(error.ReserveError('Reserve - Wavelengths %s left to be used.' % w_not_used))

        try:
            if (sr[-1] == str(0)) and (ds[-1] == str(0)):
                flag = True
            elif sr[-1] == ds[-1]:
                w_in_use.append(int(ds[-1]))
                w_not_used.remove(int(ds[-1]))
            else:
                for w in self.wavelengths:
                    if w in w_in_use:
                        w_in_use.remove(w)
                    if w not in w_not_used:
                        w_not_used.append(w)

                self.calendar.removeConnection(self.source_port, self.service_parameters.start_time, self.service_parameters.end_time)
                self.calendar.removeConnection(self.dest_port  , self.service_parameters.start_time, self.service_parameters.end_time)
                raise error.ReserveError()
                
        except error.ReserveError:
            return defer.fail(error.ReserveError('Reserve - Only reservation between similar wavelengths allowed.'))

        try:
            if self.service_parameters.bandwidth.desired:
                bandw = self.service_parameters.bandwidth.desired
            else:
                bandw = self.service_parameters.bandwidth
            nr_links = int(math.ceil(bandw/10000.0))            
            nr_used_links = nr_used_links + nr_links

            if (nr_links > 0) and (nr_used_links <= 4):
                if not flag:
                    self.wavelengths.append(int(ds[-1]))
                    for i in range(0,nr_links-1):
                        t = choice(w_not_used)
                        w_in_use.append(t)
                        w_not_used.remove(t)
                        self.wavelengths.append(t)
                else:
                    for i in range(0,nr_links):
                        t = choice(w_not_used)
                        w_in_use.append(t)
                        w_not_used.remove(t)
                        self.wavelengths.append(t)

            if nr_used_links > 4:
                w_in_use.remove(int(ds[-1]))
                w_not_used.append(int(ds[-1]))
                nr_used_links = nr_used_links - nr_links
                self.calendar.removeConnection(self.source_port, self.service_parameters.start_time, self.service_parameters.end_time)
                self.calendar.removeConnection(self.dest_port  , self.service_parameters.start_time, self.service_parameters.end_time)

                a = 4 - nr_used_links
                raise error.ReserveError()

        except error.ReserveError:
            return defer.fail(error.ReserveError('Reserve - Maximum number of links reached. Can reserve/provision %s links. ' %a))

        self.scheduler.scheduleTransition(self.service_parameters.start_time, scheduled, state.SCHEDULED)
        self.logStateUpdate('RESERVED')

        return defer.succeed(self)

    def provision(self):

        dt_now = datetime.datetime.utcnow()
        if self.service_parameters.end_time <= dt_now:
            return defer.fail(error.ProvisionError('Cannot provision connection \
            after end time. End time: %s, Current time: \
            %s.' % (self.service_parameters.end_time, dt_now)))

        self.state.switchState(state.AUTO_PROVISION) # This checks if we can switch into provision
        self.scheduler.cancelTransition() # cancel any pending scheduled switch

        def provisionSuccess(_):
            self.scheduler.scheduleTransition(self.service_parameters.end_time, lambda _ : self.terminate(), state.TERMINATING)
            self.state.switchState(state.PROVISIONED)
            self.logStateUpdate('PROVISIONED')

        def provisionFailure(err):
            log.msg('Error setting up connection: %s' % err.getErrorMessage())
            self.state.switchState(state.TERMINATED)
            self.logStateUpdate('TERMINATED')
            return err

        def doProvision(_):
            try:
                self.state.switchState(state.PROVISIONING)
                self.logStateUpdate('PROVISIONING')
            except error.StateTransitionError:
                return defer.fail(error.ProvisionError('Cannot provision connection in state %s' % self.state()))

# [DAS Network Astron] Link: 131523864, Astron_B2 -> Astron_B1 : PROVISION SCHEDULED.
            d = self.connection_manager.setupLink(self.source_port, self.dest_port) 
            d.addCallbacks(provisionSuccess, provisionFailure)
            return d

        def do_topo(_):
            s, d = self.stps() # STPS <STP DAS4:UVA20> T <STP DAS4:Astron20>
            src = re.split('(\d+)', re.split('[:]',str(s))[1])
            dest = re.split('(\d+)', re.split('[:]',str(d))[1])
            source = src[0]
            destination = dest[0]
            s_wavel = str(src[1])
            d_wavel = str(dest[1])

            try:
                if s_wavel != d_wavel:
                    raise error.InvalidRequestError()
            except:
                return defer.fail(error.InvalidRequestError('Provision - Only reservation between similar wavelengths allowed.'))

            try: 
                if self.service_parameters.bandwidth.desired:
                    bandw = self.service_parameters.bandwidth.desired
                else:
                    bandw = self.service_parameters.bandwidth
                nr_links = int(math.ceil(bandw/10000.0))

                n, topo, t = makeTopology.provision(nr_links, self.wavelengths, source, destination, self.pxc)
                log.msg('Provision - Topology change took: %s seconds' % t, system=scheduler.LOG_SYSTEM)
                return defer.succeed(None)
            except error.ProvisionError:
                return defer.fail(error.ProvisionError('Provision - Maximum number of links reached. '))

        log.msg('Provisioning connection. Start time: %s, Current time: \
        %s.' % (self.service_parameters.start_time, dt_now), system=scheduler.LOG_SYSTEM)

        if self.service_parameters.start_time <= dt_now:
            defer_provision = doProvision(None)
            defer_provision.addCallback(do_topo)
        else:
            defer_provision = self.scheduler.scheduleTransition(self.service_parameters.start_time, doProvision, state.PROVISIONING)
            defer_provision.addCallback(do_topo)
            self.logStateUpdate('PROVISION SCHEDULED')

        return defer.succeed(self), defer_provision


    def release(self):

        def releaseSuccess(_):
            self.scheduler.scheduleTransition(self.service_parameters.end_time, lambda _ : self.terminate(), state.TERMINATING)
            self.state.switchState(state.SCHEDULED)
            self.logStateUpdate('SCHEDULED')
            return self

        def releaseFailure(err):
            log.msg('Error releasing connection: %s' % err.getErrorMessage())
            self.state.switchState(state.TERMINATED)
            self.logStateUpdate('TERMINATED')
            return err

        for w in self.wavelengths:
            if w in w_in_use:
                w_in_use.remove(w)
            if w not in w_not_used:
                w_not_used.append(w)
        try:
            self.state.switchState(state.RELEASING)
            self.logStateUpdate('RELEASING')
        except error.StateTransitionError:
            return defer.fail(error.ProvisionError('Cannot release connection in state %s' % self.state()))

        self.scheduler.cancelTransition() # cancel any pending scheduled switch

        d = self.connection_manager.teardownLink(self.source_port, self.dest_port)
        d.addCallbacks(releaseSuccess, releaseFailure)
        return d

    def terminate(self):
        global nr_used_links
        global w_not_used
        global w_in_use
        
        def removeCalendarEntry():
            self.calendar.removeConnection(self.source_port, self.service_parameters.start_time, self.service_parameters.end_time)
            self.calendar.removeConnection(self.dest_port  , self.service_parameters.start_time, self.service_parameters.end_time)

        def terminateSuccess(_):
            removeCalendarEntry()
            self.state.switchState(state.TERMINATED)
            self.logStateUpdate(state.TERMINATED)
            return defer.succeed(self)

        def terminateFailure(err):
            log.msg('Error terminating connection: %s' % err.getErrorMessage())
            removeCalendarEntry() # This might be wrong :-/
            self.state.switchState(state.TERMINATED)
            self.logStateUpdate(state.TERMINATED)
            return err

        def doTerminate():
# STPS <STP DAS4:UVA20> T <STP DAS4:Astron20>
            global nr_used_links
            s, d = self.stps()
            src = re.split('(\d+)', re.split('[:]',str(s))[1])
            dest = re.split('(\d+)', re.split('[:]',str(d))[1])
            source = src[0]
            destination = dest[0]
            s_wavel = str(src[1])
            d_wavel = str(dest[1])

            if self.service_parameters.bandwidth.desired:
                bandw = self.service_parameters.bandwidth.desired
            else:
                bandw = self.service_parameters.bandwidth
            nr_links = int(math.ceil(bandw/10000.0))
            nr_used_links = nr_used_links - nr_links

            # verbreek de connecties
            n, topo, t = makeTopology.terminate(nr_links, self.wavelengths, source, destination, self.pxc)
            log.msg('Terminate - Topology change took: %s seconds' % t, system=scheduler.LOG_SYSTEM)
            for w in self.wavelengths:
                if w in w_in_use:
                    w_in_use.remove(w)
                if w not in w_not_used:
                    w_not_used.append(w)
            return defer.succeed(None)
        
        if self.state() == state.TERMINATED:
            return defer.succeed(self)

        teardown = True if self.state() == state.PROVISIONED else False
        connecties.remove(self)
        self.state.switchState(state.TERMINATING) # we can (almost) always switch to this
        self.logStateUpdate(state.TERMINATING)
        self.scheduler.cancelTransition() # cancel any pending scheduled switch

        doTerminate()

        if teardown:
            self.state.switchState(state.CLEANING)
            self.logStateUpdate(state.CLEANING)
            d = self.connection_manager.teardownLink(self.source_port, self.dest_port)
            d.addCallbacks(terminateSuccess, terminateFailure)
            return d
        else:
            return terminateSuccess(None)            

    def query(self, query_filter):
        pass
