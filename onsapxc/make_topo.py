import argparse, sys, pprint, copy
from types import *
from random import choice
from timeit import Timer

from twisted.internet import fdesc, defer
from twisted.python import log

from pxc.defaults import list_topologies

MAX_WL = 4
sites   = { 'SN039874': ['VU',1],
            'SN042680': ['ASTRON',2],
            'SN043946': ['DELFT',3],
            'SN044017': ['UVA',4]
          }

not_used = [19, 20, 21, 22]
used_links = 0
in_use = []


def update_list(topo, s):
    # 1 append 2 remove 0 block all
    if s == 1:
        list_topologies.append(topo)
    elif s == 2:
        for t in list_topologies:
            if topo in list_topologies:
                list_topologies.remove(topo)
    elif s == 0:
        list_topologies[:] = []
        list_topologies.append({'SN039874': [], 'SN042680': [], 'SN043946': [], 'SN044017': []})


# VU VU 1 19
# Number of links is chosen according to the number of given wavelenghts.
# lengte van lijst van golflengtes == aantal links
def make_topo(src, dest, nr, wl):
    # list of taken wavelengths
    in_use = []

    # maak lijst met golflengtes
    if type(wl) is list:
        newwl = wl
    else:
        in_use.extend(wl)
        temp = [i for i in range(19,23)]
        t = wl
        for i in range(0,nr):
            if temp[i] not in in_use:
                t.append(temp[i])
        temp = temp[0:nr]
        nr = int(nr)
        newwl = t

    topo = {'SN039874':[],
            'SN042680':[],
            'SN043946':[],
            'SN044017':[],
            }

    dest = dest.upper()
    src = src.upper()
    for key, value in sites.iteritems():
        # get port and SNO of source and destination
        if value[0] == dest:
            dest_SNO = key
            dest_port = value[1]
        if value[0] == src:
            src_SNO = key
            src_port = value[1]

    l = len(newwl)

    # make list for SNO to wl and port
    temp1 = [[] for i in range(l)]
    temp2 = [[] for i in range(l)]

# maak de nieuwe lijst van topologie
    for k1, v1 in sites.iteritems():
        if k1 == src_SNO:
            for i in range(0,l):
                temp1[i] = [str(newwl[i]), str(dest_port), '0.0']
            topo[k1] = temp1
        if k1 == dest_SNO:
            for i in range(0,l):
                temp2[i] = [str(newwl[i]), str(src_port), '0.0']
            topo[k1] = temp2

#    pp = pprint.PrettyPrinter()
#    pp.pprint(topo)

    return topo


# checks if topology with given arguments already exists
# return 1 and existing topology if it exists
# return 0 if not
# 0 = does not exist
def topo_exists(nr_links, wavel, source, dest):
    source = source.upper()
    dest = dest.upper()
    for k,l in sites.iteritems():
        if source == l[0]:
            s = k
        if dest == l[0]:
            d = k

    if s and d:
        for t in list_topologies:
            temp_s = ""
            temp_d = ""
            for key, value in t.iteritems():
                if key == s:
                    temp_s = s
                elif key == d:
                    temp_d = d

                if temp_s:
                    if key == d:
                        value = [item for sublist in value for item in sublist]
                        wavel = map(str, wavel)
                        if False not in [ e in value for e in wavel ] and (len(value)/len(wavel) == 3):
                            return (1,t)
                if temp_d:
                    if key == s:
                        value = [item for sublist in value for item in sublist]
                        wavel = map(str, wavel)
                        if False not in [ e in value for e in wavel ] and (len(value)/len(wavel) == 3):
                            return (1,t)
    return 0

def merge_dicts(new, old):
    new_state = copy.deepcopy(new)
    for s, t in sites.iteritems():
        for o in old[s]:
            if o not in new_state[s]:
                new_state[s].append(o)

        for n in new[s]:
            for o in old[s]:
                if n != o:
                    if (o not in new[s]) and (o not in new_state[s]):
                        new_state[s].append(o)
    return new_state


# aangeroepen door das.py (de backend).
# roept make_topo aan en geeft -1 terug als het max aantal links al bereikt is.
def provision(nr_links, w, source, dest, pxc):
    global not_used
    global in_use

    topo_new = make_topo(source, dest, nr_links, w)
    print "make_topo changetopo new"
    pp = pprint.PrettyPrinter()
    pp.pprint(topo_new)


    topo_old = pxc.getTopo(update=True)
    print "make_topo changetopo old"
    pp.pprint(topo_old)

    merged = merge_dicts(topo_new, topo_old)

    print "make_topo changetopo merged"
    pp.pprint(merged)

    update_list(topo_new, 1)
    update_list(merged, 1)
    timer = Timer(lambda: pxc.changeTopo(merged))
    t = str(timer.timeit(1))

    return nr_links, topo_new, t


def diff (old, new):
    global sites

    temp = copy.deepcopy(old) # {'SN039874': [], 'SN042680': [], 'SN043946': [], 'SN044017': []}
    for s, t in sites.iteritems():
        for o in old[s]:
            for n in new[s]:
                if (o in new[s]) and (o in temp[s]):
                    temp[s].remove(o)
    return temp

def terminate(nr_links, w, source, dest, pxc):
    source = source.upper()
    dest = dest.upper()


    pp = pprint.PrettyPrinter()
    # get old state from list
    if False:
        f = topo_exists(nr_links, w, source, dest)
        if not f:
            topo_old = make_topo(source, dest, nr_links, w)
        else:
            f1, f2 = f
            topo_old = f2
        print "topo to be terminated"
        pp.pprint(topo_old)



    # get old state from WSS
    topo_terminate = make_topo(source, dest, nr_links, w)
    nr = pxc.getTopoNr(topo_terminate)
    print "to be terminated topo nr is:", nr
    print "topo to be terminated"
    pp.pprint(topo_terminate)

    # current
    topo_current = pxc.getTopo(update=True)
    print "current topo"
    pp.pprint(topo_current)

    new = diff(topo_current, topo_terminate)
    print "newstate is"
    pp.pprint(new)

    update_list(topo_terminate, 2)
    update_list(topo_current, 2)
    if not (new == {'SN039874': [], 'SN042680': [], 'SN043946': [], 'SN044017': []}):
        update_list(new, 1)

    timer = Timer(lambda: pxc.changeTopo(new))
    t = str(timer.timeit(1))

    return nr_links, new, t



