from wss import *
from defaults import topologies

class PXCException(Exception):
    def __init__(self, value):
            self.value=value

    def __str__(self):
        return repr(self.value)


class PXC:

    def __init__(self):
        self.wss_list=[WSS("/dev/com" + str(num)) for num in range (3,7)]
        self.cur_topo=self.getTopo(update=True)

    def getSNO(self):
        """ Gets the serial numbers from the WSS objects """
        for wss in self.wss_list:
            print wss

    def getTopo(self, update=False):
        """ Returns a list of all WSS and with their reconfig array """
        if not update:
            return self.cur_topo
        else:
            topo = {}
            for wss in self.wss_list:
                name = wss.send("SNO?")
                topo[name] = [w for w in wss.getReConfArray() if w[1] != "99"]
            return topo

    def getTopoNr(self, topology=None):
        """ Look up the topology number based on a specified reconfig array
            If there's no reconfig array specified it returns the number of
            the active topology"""
        try:
            return topologies.index(topology) if topology is not None else topologies.index(self.cur_topo)
        except IndexError:
            raise PXCException("Topology index out of bounds")
        except ValueError:
            raise PXCException("Unknown topology")

    def setTopo(self, topology):
        """ Clears the current topology from the WSS by blocking all and
            setting the new topology
            WARNING: this is will interrupt existing services"""
        self.blockAll()
        if topology != 0:
            for wss in self.wss_list:
                wss.setReConfArray(topology[repr(wss)])

        self.cur_topo=self.getTopo(update=True)

    def changeTopo(self, topo_new):
        """ Takes the new topology and changes the current topology to
            the new one while leaving unchanged links alone and
            shutting down wavelengths not used by the new topology."""
        topo_old=self.getTopo()
        for wss in self.wss_list:
            switch=repr(wss)
            old_wl=[cx[0] for cx in topo_old[switch]]
            new_wl=[cx[0] for cx in topo_new[switch]]
            #get only the changing crossconnects
            cx_change = [cx for cx in topo_new[switch] if cx not in topo_old[switch]]
            #turn off all crosconnects that are not used anymore
            cx_off = [[str(wl), '99', '99.9'] for wl in old_wl if wl not in new_wl]
            wss.setReConfArray(cx_off+cx_change)
        self.cur_topo=self.getTopo(update=True)

    def blockAll(self):
        """ Clears the topology and put the PXC into the all blocked state """
        for wss in self.wss_list:
            wss.blockAll()
        self.cur_topo=self.getTopo(update=True)

    def close(self):
        """ Closes the wss connection. """
        for wss in self.wss_list:
            wss.close()
