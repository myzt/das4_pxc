#import serial
import fakewss.pserial as serial

class WSSException(Exception):
    def __init__(self, value):
        self.value=value

    def __str__(self):
        return repr(self.value)

class WSS:
    """ Represents a single WSS on the DAS4 switch
        Useful commands, SNO,RRA,RPA,RES,STR """

    errors = { "CER" : "Invalid Command",
               "AER" : "Argument Error",
               "RER" : "Range Error",
               "VER" : "Verification Error", }

    def __init__(self,portname):
        self.ser = serial.Serial(portname, 115200, timeout=1)
        self.ser.flush()
        self.serialno=self.send("SNO?")

    def __repr__(self):
        """A WSS is unique by its serial number"""
        return self.serialno

    def send(self, cmd):
        """Sends the specified command to the WSS and returns the result
           if something fails it will raise a WSSException"""
        self.ser.write(self._encode(cmd))
        self.ser.flush()
        ret = self._decode(self.ser.readline())

        if ret in self.errors.keys():
            raise WSSException("WSS sayz NO! (" + self.errors[ret] + " " + cmd +  ")");

        if ret == 'OK':
            return ret

        tail = self.ser.readline()
        if self._decode(tail) == 'OK':
            if ret == None:
                ret = self.send(cmd);
            return ret
        else:
            raise WSSException("Timeout waiting for 'OK' from WSS");

    def _calculate_checksum(self, cmd):
        """Calculates a checksum for a specified command string"""
        tot=0;
        for char in cmd:
            tot=tot + ord(char)
        return hex((tot ^ 0xffff)+1)[2:].upper()

    def _encode(self, cmd):
        """Adds a hash to a command to allow for verification"""
        return "^" + cmd + "$" + self._calculate_checksum(cmd) + "\r"


    def _decode(self, str):
        """Verifies a command string and returns the plain command
           or None for a verification failure"""
        if '$' not in str:
            return None
        res=str.strip().split('^')[1]
        res=res.strip().split('$')
        if (res[1] == self._calculate_checksum(res[0])):
            return res[0]
        else:
            return None

    def _getArray(self,cmd):
        """Parses an array structure from the WSS into a list"""
        arr=filter(None, self.send(cmd).split(";"))
        return [ line.split(',') for line in arr ]

    def setReConfArray(self, channels):
        """Takes a channel configuration and writes it to WSS"""
        ura=";".join([",".join(xc) for xc in channels])
        if ura != "":
            self.send("URA " + ura)
            self.send("RSW")

    def getReConfArray(self):
        """Gets the WSS current channel to port mapping"""
        cfg=self._getArray("RRA?")
        return cfg

    def getPendingArray(self):
        """Gets the status of the configuration"""
        cfg=self._getArray("RPA?")
        return cfg

    def blockAll(self):
        """Blocks all channels on the WSS"""
        self.send("UPA 99,99.9")

    def close(self):
        self.ser.close()
