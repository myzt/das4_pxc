#from wss import *

port_wss_map={"/dev/com3":"SN039874", "/dev/com4":"SN042680", "/dev/com5":"SN043946", "/dev/com6":"SN044017"}

class Serial:

    def __init__(self, port, baudrate = 9600, timeout = None):
        self.port = port
        self.baud = baudrate
        self.item = ""
        self.file="/tmp/" + port_wss_map[self.port]

    def _calculate_checksum(self,cmd):
        """Calculates a checksum for a specified command string"""
        tot=0;
        for char in cmd:
            # Given a string of length one, return an integer representing
            # the Unicode code point of the character when the argument is
            # a unicode object, or the value of the byte when the argument
            # is an 8-bit string. For example, ord('a') returns the integer
            # 97, ord(u'\u2020') returns 8224. This is the inverse of chr()
            # for 8-bit strings and of unichr() for unicode objects.
            tot=tot + ord(char)
        # ^XOR
        return hex((tot ^ 0xffff)+1)[2:].upper()

    def getSNO(self):
        if self.port == "/dev/com3":
            return "^SN039874$FE20\r^OK$FF66"
        elif self.port == "/dev/com4":
            return "^SN042680$FE2B\r^OK$FF66"
        elif self.port == "/dev/com5":
            return "^SN043946$FE25\r^OK$FF66"
        elif self.port == "/dev/com6":
            return "^SN044017$FE2F\r^OK$FF66"
        else:
            return None

    def getRRA(self):
        t =self._getRA()
        ura=";".join([",".join(xc) for xc in t])
        csum = self._calculate_checksum(ura)
        k = "^" + ura + "$" + csum + "\r^OK$FF66"
        return k

    def _getRA(self):
        try:
            f=open(self.file,"r")
            string_list=f.readline()
            import ast
            t=ast.literal_eval(string_list)
            f.close()
        except:
            t=[]
        return t

    def _getArray(self,str_in):
        """Parses an array structure from the WSS into a list"""
        #remove URA and hash
        str_in=str_in.replace("^URA ","").split("$")[0]
        #put all elements in a list
        arr=filter(None, str_in.split(";"))
        return [ line.split(',') for line in arr ]

    def _clearRA(self):
        f=open(self.file,"w")
        f.write(str([]))
        f.close()

    def _updateRA(self, s_ura):
        old=self._getRA()
        arr=self._getArray(s_ura)
        d={}
        for n in range(19,23):
            d[n]=[[str(n), "99","99.9"]]
            for w in old:
                if str(n) == w[0]:
                    d[n].append(w)
            for w in arr:
                if str(n)== w[0]:
                    d[n].append(w)
        arr=[]
        for n in range(19,23):
            arr.append(d[n][-1])

        arr = [w for w in arr if w[1]!="99" ]
        f=open(self.file,"w")
        f.write(str(arr))
        f.close()


    def respOK(self):
        return "^OK$FF66"

    def write(self, s): #, sno = None):
        k = ""
        if s == "^SNO?$FED1\r":
            k = self.getSNO()

        elif s == "^RRA?$FEDC\r":
            k = self.getRRA()

        elif s == "^UPA 99,99.9$FD83\r":
            self._clearRA()
            k = self.respOK()

        elif "URA" in s:
            self._updateRA(s)
            k = self.respOK()

        # command instructs the unit to implement the new channel allocation array
        elif s == "^RSW$FF04\r":
            k = self.respOK()

        self.item = k
        return k

    def flush(self):
        pass

    def flushInput(self):
        raise NotImplementedError

    def flushOutput(self):
        raise NotImplementedError

    def readline(self):
        global t
        if self.item:
            r = self.item.splitlines()
            if '\r' in self.item:
                r = self.item.splitlines()
                self.item = r[1]
                return r[0]
        else:
            self.item = ""

        return self.item

    def close(self):
        pass
